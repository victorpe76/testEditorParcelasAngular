import { TestlibPage } from './app.po';

describe('testlib App', () => {
  let page: TestlibPage;

  beforeEach(() => {
    page = new TestlibPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
