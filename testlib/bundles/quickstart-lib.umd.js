(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@ng-bootstrap/ng-bootstrap')) :
	typeof define === 'function' && define.amd ? define(['exports', '@angular/core', '@ng-bootstrap/ng-bootstrap'], factory) :
	(factory((global.quickstartLib = global.quickstartLib || {}),global.ng.core,global.ng.bootstrap));
}(this, (function (exports,_angular_core,_ngBootstrap_ngBootstrap) { 'use strict';

var LibComponent = (function () {
    function LibComponent() {
        this.name = 'Angular Library';
    }
    LibComponent.decorators = [
        { type: _angular_core.Component, args: [{
                    selector: 'my-lib',
                    template: "<div class=\"panel panel-default\"> <h2>Hola {{name}}</h2> </div> ",
                    styles: [""]
                },] },
    ];
    /**
     * @nocollapse
     */
    LibComponent.ctorParameters = function () { return []; };
    return LibComponent;
}());

var LibService = (function () {
    function LibService() {
    }
    /**
     * @return {?}
     */
    LibService.prototype.getMeaning = function () { return 42; };
    LibService.decorators = [
        { type: _angular_core.Injectable },
    ];
    /**
     * @nocollapse
     */
    LibService.ctorParameters = function () { return []; };
    return LibService;
}());

var LibModule = (function () {
    function LibModule() {
    }
    LibModule.decorators = [
        { type: _angular_core.NgModule, args: [{
                    declarations: [LibComponent],
                    imports: [_ngBootstrap_ngBootstrap.NgbModule.forRoot()],
                    providers: [LibService],
                    exports: [LibComponent]
                },] },
    ];
    /**
     * @nocollapse
     */
    LibModule.ctorParameters = function () { return []; };
    return LibModule;
}());

/**
 * Generated bundle index. Do not edit.
 */

exports.LibComponent = LibComponent;
exports.LibService = LibService;
exports.LibModule = LibModule;
exports.NgbModule = _ngBootstrap_ngBootstrap.NgbModule;

Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=quickstart-lib.umd.js.map
