import { Component, Injectable, NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

var LibComponent = (function () {
    function LibComponent() {
        this.name = 'Angular Library';
    }
    LibComponent.decorators = [
        { type: Component, args: [{
                    selector: 'my-lib',
                    template: "<div class=\"panel panel-default\"> <h2>Hola {{name}}</h2> </div> ",
                    styles: [""]
                },] },
    ];
    /**
     * @nocollapse
     */
    LibComponent.ctorParameters = function () { return []; };
    return LibComponent;
}());

var LibService = (function () {
    function LibService() {
    }
    /**
     * @return {?}
     */
    LibService.prototype.getMeaning = function () { return 42; };
    LibService.decorators = [
        { type: Injectable },
    ];
    /**
     * @nocollapse
     */
    LibService.ctorParameters = function () { return []; };
    return LibService;
}());

var LibModule = (function () {
    function LibModule() {
    }
    LibModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [LibComponent],
                    imports: [NgbModule.forRoot()],
                    providers: [LibService],
                    exports: [LibComponent]
                },] },
    ];
    /**
     * @nocollapse
     */
    LibModule.ctorParameters = function () { return []; };
    return LibModule;
}());

/**
 * Generated bundle index. Do not edit.
 */

export { LibComponent, LibService, LibModule, NgbModule };
//# sourceMappingURL=quickstart-lib.es5.js.map
