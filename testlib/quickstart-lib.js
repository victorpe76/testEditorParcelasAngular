import { Component, Injectable, NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap/index';

class LibComponent {
    constructor() {
        this.name = 'Angular Library';
    }
}
LibComponent.decorators = [
    { type: Component, args: [{
                selector: 'my-lib',
                template: "<div class=\"panel panel-default\"> <h2>Hola {{name}}</h2> </div> ",
                styles: [""]
            },] },
];
/**
 * @nocollapse
 */
LibComponent.ctorParameters = () => [];

class LibService {
    constructor() { }
    /**
     * @return {?}
     */
    getMeaning() { return 42; }
}
LibService.decorators = [
    { type: Injectable },
];
/**
 * @nocollapse
 */
LibService.ctorParameters = () => [];

class LibModule {
}
LibModule.decorators = [
    { type: NgModule, args: [{
                declarations: [LibComponent],
                imports: [NgbModule.forRoot()],
                providers: [LibService],
                exports: [LibComponent]
            },] },
];
/**
 * @nocollapse
 */
LibModule.ctorParameters = () => [];

/**
 * Generated bundle index. Do not edit.
 */

export { LibComponent, LibService, LibModule, NgbModule };
//# sourceMappingURL=quickstart-lib.js.map
