import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TesterpvisorComponent } from './testerpvisor.component';

describe('TesterpvisorComponent', () => {
  let component: TesterpvisorComponent;
  let fixture: ComponentFixture<TesterpvisorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TesterpvisorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TesterpvisorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
