import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-testerpvisor',
  templateUrl: './testerpvisor.component.html',
  styleUrls: ['./testerpvisor.component.css']
})
export class TesterpvisorComponent implements OnInit {

  private parcelasinput: any;
  private parcelas: any =[];
  constructor() { }

  ngOnInit() {
  }

  private cargarparcelas(){
    this.parcelas =this.readParcelasFromjsonArray(JSON.parse(this.parcelasinput));
  }

  readParcelasFromjsonArray(jsonpeticion): any[] {
    let tmpparcelas = [];



    //Recorremos la variable jsonrecintosmismopueblo para ir metiendo parcelas
    jsonpeticion.forEach(recinto => {

      console.log(recinto)

      let parcela= {} as any;
      parcela.idparcela = recinto.Provincia +","
      +recinto.Municipio+","+recinto.Agregado+","+
      recinto.Zona+","+recinto.Poligono+","+recinto.Parcela;
      parcela.color = recinto.ColorHex;
      parcela.detalle = recinto;

      tmpparcelas.push(parcela);

    });
    return tmpparcelas;
  }
}
