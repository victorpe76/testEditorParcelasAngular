import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestteledeteccionComponent } from './testteledeteccion.component';

describe('TestteledeteccionComponent', () => {
  let component: TestteledeteccionComponent;
  let fixture: ComponentFixture<TestteledeteccionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestteledeteccionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestteledeteccionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
