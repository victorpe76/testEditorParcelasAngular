import { Component, OnInit, ViewChild, SimpleChanges } from '@angular/core';
import { OsmeditorComponent, ParcelaModificada } from 'globalcaja-angular-lib';

@Component({
  selector: 'app-testeditor',
  templateUrl: './testeditor.component.html',
  styleUrls: ['./testeditor.component.css']
})
export class TesteditorComponent implements OnInit {



  @ViewChild('editor') editorparcela: OsmeditorComponent;
  editarsigpacid='';
  verrecintos: boolean;
  consultalocalidad: string;
  editarcoordenadas:number[][];
  parcelas=new Array<ParcelaModificada>();
  atributosparcela:any={};
  title = 'Editor de parcelas';
  parcelaitem ={sigpacid:'37,900,0,0,2,91'};

  constructor() { }

  ngOnInit() {
  }

  parcelamodificada(evento:any) {
    console.log('Recibo un evento que es una parcela');
      this.parcelas.push(evento)
    console.log(evento);
  }
  parcelaseleccionada(event:any){
    console.log('Ha seleccionado una parcela');
    this.atributosparcela=event;
  }
  ngOnChages(cambios: SimpleChanges) {
    console.log('Ha cambiado algo');
  }

  editarparcela() {
   // this.editarsigpacid=this.parcelaitem.sigpacid;
    this.editorparcela.editarParcelaAgraria(this.parcelaitem.sigpacid);
  }
  onEditParcela(parcela: ParcelaModificada) {
    this.editorparcela.editarCoordenadasParcela(this.parcelaitem.sigpacid,parcela.coordenadas)

}
}
