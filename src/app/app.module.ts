import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { OsmeditorModule, TeledeteccionModule } from 'globalcaja-angular-lib';
import { TesteditorComponent } from './testeditor/testeditor.component';
import { TestteledeteccionComponent } from './testteledeteccion/testteledeteccion.component';
import { RouterModule, Routes } from '@angular/router';
import { ChartsModule } from 'ng2-charts';
import { MatInputModule } from '@angular/material';
import { TesterpvisorComponent } from './testerpvisor/testerpvisor.component';
const appRoutes: Routes = [
  { path: 'testeditor', component: TesteditorComponent },
  { path: 'testteledeteccion',      component: TestteledeteccionComponent },
  { path: 'testerpvisor',      component: TesterpvisorComponent }]


@NgModule({
  declarations: [
    AppComponent,
    TesteditorComponent,
    TestteledeteccionComponent,
    TesterpvisorComponent
  ],
  imports: [
  NgbModule.forRoot(),
    BrowserModule,
    FormsModule,
    HttpModule,
    OsmeditorModule,
    ChartsModule,
    MatInputModule,
    RouterModule.forRoot(
      appRoutes),
    TeledeteccionModule.forRoot(
      { provide: 'SENTINEL_URLS', useValue: {
        URL_BASE_DATA : "https://ya32ayry19.execute-api.eu-west-1.amazonaws.com/production/sentinel",
        URL_BASE_NDVI_TILER :"https://ge2294g24e.execute-api.eu-west-1.amazonaws.com/production/sentinel",
        URL_BADE_NDVI :"https://ya32ayry19.execute-api.eu-west-1.amazonaws.com/production"

    }}
  ),
  OsmeditorModule.forRoot(
    { provide: 'FEGA_URLS', useValue: {
      URL_BASE : "http://aidiapp.com/proxysigpac/sdg/",
      URL_BASE_QUERY :"http://aidiapp.com/proxysigpac/VectorSDG/query/ParcelaBox/"
  }}
)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
