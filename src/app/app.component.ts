import { ParcelaModificada } from 'globalcaja-angular-lib';
import { Parcela } from 'globalcaja-angular-lib';
import { Component, SimpleChanges, ViewChild } from '@angular/core';
import { OsmeditorComponent, OsmvisorComponent } from 'globalcaja-angular-lib';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

}

