export { OsmeditorComponent } from './src/osmeditor/osmeditor.component';
export { OsmvisorComponent } from './src/osmeditor/osmvisor.component';
export { OsmeditorModule } from './src/module';
export { VisorTeledeteccionComponent } from './src/teledeteccion/osmvisortele.component';
export { TeledeteccionModule } from './src/teledeteccion/teledeteccion.module';
export * from './src/osmeditor/parcelamodificada.model';
export * from './src/osmeditor/sigpacutils/parcela.model';
