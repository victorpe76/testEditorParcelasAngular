#Test Editor Parcelas Lib

Esta es librería de utilidad del visor sigpac que contiene dos componentes:

- El componente angular editor de parcelas SIGPAC, que permite una vez recibido los datos de identificación de unha parcela modificar su contorno y retornar el polígono resultante en forma de array vectorial de coordenadas.
- El componente selector de parcelas que permite explorar el mapa, buscar una localidad y seleccionar una parcela obteniendo en respuesta todos los atributos de la misma que figuran en catastro.

## Instalación de librería
Para instalar el componente basta copiar la carpeta en el equipo y ejecutar el siguiente comando dentro del proyecto donde queramos instalar el componente:
- `npm install ./rutarelativa/a/libreria/`

## Uso del componente osmeditor
Para utilizar el editor de parcelas incluiremos la etiqueta `<app-osmeditor>` y la referenciaremos mediante un id (por ejemplo #editordeparcelas).
De esta manera podremos acceder a los dos métodos públicos para editar una parcela:

- `editarParcelaAgraria(parcela: any)`: Recibe el identificador de parcela que se quiere editar con el formato PROV,MUNICIPIO,ZONA,AGREGADO,POLIGONO,PARCELA
- `editarCoordenadasParcela(parcela: any,coordenadasparcela: number[][])`: Recibe el identificador de parcela original( con el formato PROV,MUNICIPIO,ZONA,AGREGADO,POLIGONO,PARCELA) y el conjunto de coordenadas que conformen el polígono que se quiere editar

El componente incluye el evento modificada:
- `modificada` Establece la función a ejecutar cuando se confirme la modificación de la parcela

El siguiente extracto muestra la integración del editor en una plantilla html de angular:
```html
<label>Referencia de parcela SIGPAC: </label>
    <input [(ngModel)]="parcelaitem.sigpacid" placeholder="PROV,MUNICIPIO,ZONA,AGREGADO,POLIGONO,PARCELA" />
   <button (click)="editarparcela()">Editar</button>
  <h3>Lista de parcelas editadas (clic para volver a editar)</h3>
   <ul>
   		<li *ngFor="let parcela of parcelas" (click)="onEditParcela(parcela)">
   			Parcela editada: <span>{{parcela.area}} ha</span>
   		</li>
   </ul>
<app-osmeditor #editor (modificada)="parcelamodificada($event)"></app-osmeditor>
```
```javascript
@ViewChild('editor') editorparcela: OsmeditorComponent;
...
editarparcela() {

  this.editorparcela.editarParcelaAgraria(this.parcelaitem.sigpacid);
}
onEditParcela(parcela: ParcelaModificada) {
  this.editorparcela.editarCoordenadasParcela(this.parcelaitem.sigpacid,parcela.coordenadas)

}
```

### El evento modificada
Como hemos señalado arriba este evento se dispara una vez que se haya confirmado la modificación de la parcela. El evento enviado en la función es un objeto del tipo ParcelaModificada que contiene los siguientes campos:
- `coordenadas`: Array vectorial bidimensional que contiene el listado de coordenadas (latitud, longitud) que conforman el polígono resultante
- `area`: Un número que indica el area actual del polígono
- `perimetro`: Número que indica el perímetro total polígono

## Uso del componente osmvisor
Para utilizar el visor de parcelas incluiremos la etiqueta `<app-osmvisor>` y la referenciaremos mediante un id (por ejemplo #visor).
De esta manera podremos acceder al método publico que permite realizar una búsqueda por localidad. El componente incluye los siguientes parámetros:

- `recintos`: Atributo boleano que permite indicar si en el mapa se muestran parcelas (valor false por defecto) o se muestran recintos (valor true)
- `centermap`: Posición inicial del mapa en formato cadena latitud,longitud


El componente incluye el evento modificada:
- `seleccionada` Establece la función a ejecutar cuando se seleccione una parcela o recinto. Como argumento de la función se envía el conjunto de atributos de la parcela seleccionada.

El siguiente extracto muestra la integración del editor en una plantilla html de angular:
```html
<div class="row">

  <div class="col-md-6">
    <app-osmvisor #visor [recintos]="verrecintos" (seleccionada)="parcelaseleccionada($event)" centermap="-5.6630777,40.9705077"></app-osmvisor>
  </div>
  <div class="col-md-6">
    <h3>Información de parcela seleccionado</h3>
      <ul>
        <li>PROVINCIA: {{atributosparcela.PROVINCIA}}</li>
        <li>MUNICIPIO: {{atributosparcela.MUNICIPIO}}</li>
        <li>ZONA: {{atributosparcela.ZONA}}</li>
        <li>AGREGADO: {{atributosparcela.AGREGADO}}</li>
        <li>POLÍGONO: {{atributosparcela.POLIGONO}}</li>
        <li>PARCELA: {{atributosparcela.PARCELA}}</li>
        <li>RECINTO: {{atributosparcela.RECINTO}}</li>
      </ul>
      <h4>RECINTOS</h4>
      <table *ngIf="atributosparcela.RECINTOS">
        <thead>
          <tr>
            <th>RECINTO</th>
            <th>SUPERFICIE</th>
            <th>PENDIENTE</th>
            <th>USO</th>
            <th>PASTOS (%)</th>
            <th>PASTOS (HA)</th>
            <th>COEF. REGADIO</th>
            <th>INCIDENCIA</th>
            <th>REGION</th>
          </tr>
        </thead>
        <tbody>
          <tr *ngFor="let r of atributosparcela.RECINTOS">
              <td>{{r.RECINTO}}</td>
              <td>{{r.SUPERFICIE}}</td>
              <td>{{r.PENDIENTE}}</td>
              <td>{{r.PASTOSPERCENT}}</td>
              <td>{{r.PASTOSHA}}</td>
              <td>{{r.COEFREGADIO}}</td>
              <td>{{r.CODINCIDENCIA}}</td>
              <td>{{r.REGION}}</td>
          </tr>
        </tbody>
      </table>
  </div>
</div>
```
```javascript

parcelaseleccionada(event:any){
    console.log('Ha seleccionado una parcela');
    this.atributosparcela=event;
  }
```

