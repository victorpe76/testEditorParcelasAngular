import { FegarequestsService } from './sigpacutils/fegarequests.service';
import { Parcela } from './sigpacutils/parcela.model';
import { OnInit, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';
import * as ol from 'openlayers';
export declare class ErpvisorComponent implements OnInit, OnChanges {
    private service;
    closeResult: string;
    map: ol.Map;
    overlay: ol.Overlay;
    content_popup: any;
    feature: ol.Feature;
    featureoriginal: ol.Feature;
    source: ol.source.Vector;
    parcelascargadas: Array<Parcela>;
    parcelasselected: any;
    config: any;
    centermap: string;
    recintos: boolean;
    seleccionada: EventEmitter<any>;
    parceladetallada: any[];
    private currentparcela;
    private currentparceladetail;
    constructor(service: FegarequestsService);
    ngOnInit(): void;
    ngAfterViewInit(): any;
    initMap(): void;
    buscarlocalidad(ciudad: string): void;
    ngOnChanges(changes: SimpleChanges): void;
    onSelectParcela(evt: any): void;
    onMoveMapa(): void;
    pintarparcelasdetalladas(): void;
}
