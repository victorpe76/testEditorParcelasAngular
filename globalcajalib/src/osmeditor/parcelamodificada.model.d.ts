export declare class ParcelaModificada {
    coordenadas: number[][];
    area: number;
    perimetro: number;
}
