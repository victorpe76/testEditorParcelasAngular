import { Parcela } from './parcela.model';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
export declare class FegarequestsService {
    private http;
    sentinelurl: any;
    private URL_BASE;
    private URL_BASE_QUERY;
    constructor(http: Http, sentinelurl: any);
    getParcelas(coords1: any, coords2: any, recintos: boolean): Promise<Array<Parcela>>;
    queryLocationByAddress(ciudad: string): Promise<any>;
    getExtendedAtributos(atributos: any, recinto: boolean): Promise<any>;
    getParcela(params: string): Promise<Parcela>;
}
