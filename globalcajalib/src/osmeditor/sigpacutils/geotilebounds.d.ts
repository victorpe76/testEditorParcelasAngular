export declare class GeoTileBounds {
    srid: any;
    YMin: any;
    YMax: any;
    XMin: any;
    XMax: any;
    constructor(xMax: any, xMin: any, yMax: any, yMin: any, srid: any);
}
