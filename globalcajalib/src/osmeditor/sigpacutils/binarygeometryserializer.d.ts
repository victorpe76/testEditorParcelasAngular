import { ParcelaBox } from './parcelabox';
export declare class BinaryGeometrySerializer {
    ReadLineString: any;
    atributenames: string[];
    data: [number];
    constructor(data: [number]);
    read(): Array<ParcelaBox>;
    private ReadEntity(s);
    ReadFeatures(): any[];
    private ReadFeatureVector(b, a);
    private ReadPoint(b);
    private ReadMultiPoint(b);
    private ReadMultiLineString(b);
    private ReadMultiPolygon(b);
    private ReadPoints(s);
    private ReadPolygon(s);
    private ReadRing(s);
    private ReadOpenLayersAttributes(s, a);
    private ReadEntityAttributes(s);
}
