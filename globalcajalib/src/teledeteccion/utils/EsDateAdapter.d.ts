import { NativeDateAdapter } from '@angular/material';
export declare class EsDateAdapter extends NativeDateAdapter {
    getFirstDayOfWeek(): number;
}
