import { FegarequestsService } from './../osmeditor/sigpacutils/fegarequests.service';
import { Parcela } from './../osmeditor/sigpacutils/parcela.model';
import { OnInit, EventEmitter, SimpleChanges, NgZone, ElementRef } from '@angular/core';
import * as ol from 'openlayers';
import { SentinelDataService } from './service/sentinel-data.service';
import { SentinelScene } from '../models/sentinel.scene';
import { NdviData } from '../models/ndvi.data';
import { MatDatepickerInputEvent, DateAdapter, MatSnackBar } from "@angular/material";
import { DatePipe } from '@angular/common';
import { Observable } from 'rxjs/Observable';
import { MatSliderChange } from '@angular/material/slider';
export declare class VisorTeledeteccionComponent implements OnInit {
    private service;
    private sentinel;
    private zone;
    snackBar: MatSnackBar;
    private datePipe;
    private adapter;
    select: ol.interaction.Select;
    static URL_TILER_BASE: string;
    currentEscena: SentinelScene;
    currentGeometry: ol.geom.Geometry;
    sentinelLayer: ol.layer.Tile;
    map: ol.Map;
    feature: ol.Feature;
    featureoriginal: ol.Feature;
    source: ol.source.Vector;
    parcelascargadas: Array<Parcela>;
    parcelasselected: any;
    config: any;
    listaescenas: SentinelScene[];
    sentinelfunction: string;
    loadingscenes: boolean;
    loadingtiles: boolean;
    loadingndvi: boolean;
    ndvidata: NdviData;
    sigpacid: string;
    sentinelawsprefix: string;
    sentinelawsdata: any;
    currentsigpacid: string;
    fourweeks: any;
    fourweeks_year: any;
    threemonths: any;
    threemonths_year: any;
    fechaseleccionada: Date;
    currentndviindex: number;
    currentndetaildviindex: number;
    utmstr: string;
    fechasdisponibles: Date[];
    fechadate: any;
    singlefeature: boolean;
    añoActual: number;
    añoAnterior: number;
    grupopanel: string;
    imagedata: string;
    detailedimage: HTMLImageElement;
    canvasparcela: ElementRef;
    canvascontainer: ElementRef;
    selectorpixel: ElementRef;
    pixelndvidata: any;
    eventLocation: any;
    context: any;
    satInfoToShow: any;
    infosatellite: any[];
    date_latest: Date;
    localeDate_latest: String;
    date_first: Date;
    localeDate_first: String;
    dates_sorted: String[];
    sorted_ms: number[];
    fulldates: Date[];
    num_items_slider: number;
    centermap: string;
    recintos: boolean;
    configuracion: any;
    seleccionada: EventEmitter<any>;
    change: EventEmitter<any>;
    input: EventEmitter<any>;
    slider_valueDate: String;
    slider_valueID: number;
    constructor(service: FegarequestsService, sentinel: SentinelDataService, zone: NgZone, snackBar: MatSnackBar, datePipe: DatePipe, adapter: DateAdapter<any>);
    ngOnInit(): void;
    /**
     * Return the location of the element (x,y) being relative to the document.
     *
     * @param {Element} obj Element to be located
     */
    getElementPosition(obj: any): {
        x: number;
        y: number;
    };
    /**
     * return the location of the click (or another mouse event) relative to the given element (to increase accuracy).
     * @param {DOM Object} element A dom element (button,canvas,input etc)
     * @param {DOM Event} event An event generate by an event listener.
     */
    getEventLocation(element: any, event: any): {
        x: number;
        y: number;
    };
    drawImageOnCanvas(): void;
    picarPixel(e: any): void;
    rgbToHex(r: number, g: number, b: number): string;
    ngAfterViewInit(): any;
    refreshMapa(): void;
    initMap(): void;
    buscarlocalidad(ciudad: string): void;
    ngOnChanges(changes: SimpleChanges): void;
    onSelectParcela(feature: any): void;
    onSelectParcelaOnMapa(evt: any): void;
    onMoveMapa(): void;
    comprobarDisponibilidadSatelite(): void;
    getCustomDates(data: any): void;
    cargarParcelarioSIGPAC(): void;
    loadEscena(escena: SentinelScene): void;
    checkEscenaAndCrop(): void;
    loadNDVI(): void;
    loadNDVI_year(): void;
    loadNDVI_month(year: number, month: number): Observable<any>;
    loadNDVI_threemonths(): void;
    loadNDVI_threemonths_year(): void;
    chartClicked(e: any): void;
    cerrarinfosat(): void;
    calculateNDVI(data: any): void;
    seleccionarEscena(evt: any): void;
    seleccionarFecha(evt: MatDatepickerInputEvent<Date>): void;
    compruebaEscena(fecha: Date): void;
    getUrlSentinel(): string;
    localizarParcela(pid: string): void;
    filtrarfechasatelites: (fecha: Date) => Date;
    mueveScroll(evento: MatSliderChange): void;
    cambiaScroll(evento: MatSliderChange): void;
    formatLabel(value: number | null): string | number;
}
