import { Provider, ModuleWithProviders } from '@angular/core';
export declare class TeledeteccionModule {
    static forRoot(sentinelurls: Provider): ModuleWithProviders;
}
