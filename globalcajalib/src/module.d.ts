import { Provider, ModuleWithProviders } from '@angular/core';
export declare class OsmeditorModule {
    static forRoot(fegaurls: Provider): ModuleWithProviders;
}
